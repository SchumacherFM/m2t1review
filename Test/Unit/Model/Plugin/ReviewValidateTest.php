<?php

namespace SchumacherFM\M2T1Review\Test\Unit\Model\Plugin;

use SchumacherFM\M2T1Review\Model\Plugin\ReviewValidate;

class ReviewValidateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \SchumacherFM\M2T1Review\Model\Plugin\ReviewValidate
     */
    protected $reviewPlugin;

    /**
     * @var \Magento\Review\Model\Review|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $reviewMock;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $this->reviewMock = $this->getMock('Magento\Review\Model\Review', ['getNickname'], [], '', false);

        $this->reviewPlugin = new ReviewValidate();
    }

    /**
     * @return void
     */
    public function testafterValidateSuccess()
    {
        $this->reviewMock->expects($this->once())
            ->method('getNickname')
            ->willReturn('Magento');

        $this->assertTrue($this->reviewPlugin->afterValidate($this->reviewMock, true));
    }

    /**
     * @return void
     */
    public function testafterValidateFailure()
    {
        $this->reviewMock->expects($this->once())
            ->method('getNickname')
            ->willReturn('Mage-nto');

        $expect = array(
            0 => 'Nickname cannot contain dashes (-).',
        );

        $this->assertEquals($expect, $this->reviewPlugin->afterValidate($this->reviewMock, true));
    }
}
