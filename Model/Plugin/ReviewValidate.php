<?php

namespace SchumacherFM\M2T1Review\Model\Plugin;

use Magento\Framework\Phrase;

class ReviewValidate
{
    /**
     * @param \Magento\Review\Model\Review $subject
     * @param bool|string[]                $result
     *
     * @return bool|string[]
     */
    public function afterValidate(\Magento\Review\Model\Review $subject, $result)
    {
        $errors = [];
        if (strpos($subject->getNickname(), '-') !== false) {
            $errors[] = (new Phrase('Nickname cannot contain dashes (-).'))->__toString();
        }

        if (empty($errors) && $result === true) {
            return true;
        }

        if (is_array($result)) {
            return array_merge($errors, $result);
        }
        return $errors;
    }
}
