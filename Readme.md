## Magento 2 Trained Partner Program: Developer Exercises 

# Unit 1 Review

When submitting a review on the product view page, make sure that the username does not contain dashes. If it does, show an error message. (Training1_Review)

